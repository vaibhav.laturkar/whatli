from ._countvector_lang import CountVectorLanguage
from ._tfidf_lang import TFIDFVectorLanguage
from ._bpemblang import BytePairLanguage
from ._gensim_lang import GensimLanguage

from whatlies.error import NotInstalled



from ._tfhub_lang import TFHubLanguage
from ._convert_lang import ConveRTLanguage
from ._sentence_encode_lang import UniversalSentenceLanguage
from ._hftransformers_lang import HFTransformersLanguage, LaBSELanguage
from ._sense2vec_lang import Sense2VecLanguage
from ._fasttext_lang import FasttextLanguage
from ._floret_lang import FloretLanguage
from ._spacy_lang import SpacyLanguage
from ._sentencetfm_lang import SentenceTFMLanguage
#from ._diet_lang import DIETLanguage


__all__ = [
    "SpacyLanguage",
    "FasttextLanguage",
    "CountVectorLanguage",
    "TFIDFVectorLanguage",
    "BytePairLanguage",
    "GensimLanguage",
    "ConveRTLanguage",
    "TFHubLanguage",
    "HFTransformersLanguage",
    "UniversalSentenceLanguage",
    "SentenceTFMLanguage",
    "LaBSELanguage",
    #"DIETLanguage",
    "FloretLanguage",
]
